import datetime

from django.utils import timezone
from django.test import TestCase
from django.core.urlresolvers import reverse

from .models import person


def create_person(person_text, days):
    """
    Creates a person with the given `person_text` and published the
    given number of `days` offset to now (negative for persons published
    in the past, positive for persons that have yet to be published).
    """
    time = timezone.now() + datetime.timedelta(days=days)
    return person.objects.create(person_text=person_text,
                                   pub_date=time)


class personViewTests(TestCase):
    def test_index_view_with_no_persons(self):
        """
        If no persons exist, an appropriate message should be displayed.
        """
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['latest_person_list'], [])

    def test_index_view_with_a_past_person(self):
        """
        persons with a pub_date in the past should be displayed on the
        index page.
        """
        create_person(person_text="Past person.", days=-30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_person_list'],
            ['<person: Past person.>']
        )

    def test_index_view_with_a_future_person(self):
        """
        persons with a pub_date in the future should not be displayed on
        the index page.
        """
        create_person(person_text="Future person.", days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, "No polls are available.",
                            status_code=200)
        self.assertQuerysetEqual(response.context['latest_person_list'], [])

    def test_index_view_with_future_person_and_past_person(self):
        """
        Even if both past and future persons exist, only past persons
        should be displayed.
        """
        create_person(person_text="Past person.", days=-30)
        create_person(person_text="Future person.", days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_person_list'],
            ['<person: Past person.>']
        )

    def test_index_view_with_two_past_persons(self):
        """
        The persons index page may display multiple persons.
        """
        create_person(person_text="Past person 1.", days=-30)
        create_person(person_text="Past person 2.", days=-5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_person_list'],
            ['<person: Past person 2.>', '<person: Past person 1.>']
        )

class personIndexDetailTests(TestCase):
    def test_detail_view_with_a_future_person(self):
        """
        The detail view of a person with a pub_date in the future should
        return a 404 not found.
        """
        future_person = create_person(person_text='Future person.',
                                          days=5)
        response = self.client.get(reverse('polls:detail',
                                   args=(future_person.id,)))
        self.assertEqual(response.status_code, 404)

    def test_detail_view_with_a_past_person(self):
        """
        The detail view of a person with a pub_date in the past should
        display the person's text.
        """
        past_person = create_person(person_text='Past person.',
                                        days=-5)
        response = self.client.get(reverse('polls:detail',
                                   args=(past_person.id,)))
        self.assertContains(response, past_person.person_text,
                            status_code=200)